#include "Agents.hpp"

int randInt(int min, int max)
{
    /* Return a random int from min to max.
     * Notice that the result may include both min and max.
     * i.e. return a number in [min, max].
     */
    if (max < min)
    {
        int temp = max;
        max = min;
        min = temp;
    }
    return rand() % (max - min + 1) + min;
}


Agent::Agent()
{
    setName("Virtual Agent");
}

Agent::~Agent()
{
}

void
Agent::setName(char const *name)
{
    this->m_name = name;
}

char const *
Agent::getName()
{
    return this->m_name;
}


RandomAgent::RandomAgent()
{
    setName("Random Agent");
}

RandomAgent::~RandomAgent()
{
}

char
RandomAgent::getAction(State gameState)
{
    std::vector<char> actions = gameState.getActions();
    int index = randInt(0, actions.size()-1);
    return actions[index];
}


MonteCarloAgent::MonteCarloAgent()
{
    m_trial_time = 100;
    setName("Monte Carlo Agent");
}

MonteCarloAgent::~MonteCarloAgent()
{
}

void
MonteCarloAgent::setTrialTime(int trial_time)
{
    this->m_trial_time = trial_time;
}

int
MonteCarloAgent::getTrialTime()
{
    return this->m_trial_time;
}

int
MonteCarloAgent::randomlyPlayGameOnce(State gameState)
{
    RandomAgent agent;
    int total_score = 0;
    State state = gameState;

    while (!state.isDeadState())
    {
        char action = agent.getAction(state);
        int step_score = 0;
        state = state.getSuccessor(action, &step_score);
        total_score += step_score;
        state = state.getAddTileSuccessor();
        
    }

    return total_score;
}

char
MonteCarloAgent::getAction(State gameState)
{
    int TRIAL_TIME = getTrialTime();

    std::vector<char> actions = gameState.getActions();
    std::vector<int> scores;

    for (int i = 0; i < actions.size(); i++)
    {
        int sum_score = 0;

        for (int j = 0; j < TRIAL_TIME; j++)
        {
            int cur_game_score = 0;
            State nextState = gameState.getSuccessor(actions[i], &cur_game_score).getAddTileSuccessor();
            cur_game_score += randomlyPlayGameOnce(nextState);
            sum_score += cur_game_score;
        }
        
        scores.push_back(sum_score);
        
    }

    int max_score = 0;
    int max_index = 0;
    for (int i = 0; i < scores.size(); i++)
    {
        if (scores[i] > max_score)
        {
            max_score = scores[i];
            max_index = i;
        }
            
    }
    
    return actions[max_index];
    
}

MonteCarloAgentWithTimer::MonteCarloAgentWithTimer()
{
    setName("Monte Carlo Agent With Timer");
}

MonteCarloAgentWithTimer::~MonteCarloAgentWithTimer()
{
}

void
MonteCarloAgentWithTimer::setTimeLimit(double time_limit)
{
    this->m_action_time_limit = time_limit;
}

double
MonteCarloAgentWithTimer::getTimeLimit()
{
    return this->m_action_time_limit;
}

/* ********** TASK 5  Monte Carlo Agent With Timer! ************ *
 * Monte Carlo Agent has upgraded this time! Your task is to     *
 * equip him with a timer!                                       *
 *                                                               *
 * Implement [ char MonteCarloAgentWithTimer::getAction(State    *
 * gameState) ].                                                 *
 *                                                               *
 * Use the Timer you just built!                                 *
 * ************************************************************* */

char
MonteCarloAgentWithTimer::getAction(State gameState)
{
    /* Get an action given a game state.
     * For Monte Carlo Agent With Timer, you should
     *  1. From current game state, take a valid action.
     *  2. After taking the action, play the game randomly for a fixed time period. 
     * Here you should use [ double MonteCarloAgentWithTimer::getTimeLimit() ] to get 
     * the total time you have for simulation. This means that for each action, the posible
     * time to use is no longer than the total time divided by the number of actions.
     *  3. Repeat step 1 for many times until you have tried all the possible actions.
     *  4. Return the action that get the highest average score during your random play.
     * 
     * HINT: You can refer to [ char MonteCarloAgent::getAction(State gameState) ] for most 
     * part of the implementation.
     */

    // YOUR CODE HERE

}