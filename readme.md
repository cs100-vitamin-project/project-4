# Project 4 - Timer

Monte Carlo Agent performs great, but the slow process may drive you mad. This time you will build a simple timer, and use it to imporve your agent by achieving a better balance between performance and efficiency.

After this project, you should:
- be able to build a simple timer
- know how to use it to test algorithms and data structures
- improve the agent you built last time

This project is adapted from CS100_Tutorial_7, 2018, ShanghaiTech.

The whole project is based on game 2048. If you are not familiar with this popular game, please refer to [this website](https://play2048.co//).

---
## Files

There are 5 tasks in total.

Files to create:

    Timer.hpp
    Timer.cpp

Files to modify:

    Agents.cpp
    task1.cpp
    task3.cpp
    task4.cpp

Files to read:

    task2.cpp

Files that should not be modified:

    State.cpp
    State.hpp
    main.cpp
    platform.cpp
    platform.hpp
    Agents.hpp

---
## Task 1 - std::chrono

In this task, you should implement the function `void task1()` in `task1.cpp`.

Firstly, search on the Internet to find what is `std::chrono`. Find out the basic usage. Just figure out how to
- Get time-­points
- Extract durations
- Express durations in microseconds, and cast them to an int

Then fill in the places with ... in the file `task1.cpp`.
Your goal is to test how much time it could take by running `dummyFunction(40)`.

When you finish, type

    g++ task1.cpp -o task1

in the console to compile, and get `task1` or `task1.exe`.

Then run (for Windows cmd)

    task1.exe

to see the result.

---
## Task 2 - Implement a Timer class

In this task, you should create new files `Timer.hpp` and `Timer.cpp` in the current folder. Notice that no helper code will be provided! It's on your own.

The interface of `Timer` is provided as below:

```c++
class Timer {  
public:
    Timer( bool start = false );
    virtual ~Timer();

    void start();
    void stop( bool restart = false );
    void stop( size_t iterations,bool restart = false );
    void reset();

    double totalTime();
    double averageTime();

    std::list<double>::iterator begin();
    std::list<double>::iterator end();

    int getIterations();
    void sort();
...
}
```

Think about what private variable to choose and what type should the lap time container be (vector or linkedlist, or something else).

`begin()` should return the iterator to first element in lap-time container. `end()` should return the iterator to last element in lap-­time container.

Do not forget the include-guard! When you finish, type

    g++ task2.cpp Timer.cpp -o task2

in the console to compile, and get `task2` or `task2.exe`.

Then run (for Windows cmd)

    task2.exe

to see the result.

Try to get everything correct at the very first time! Otherwise you may struggle in the following tasks.

---
## Task 3 - Filling lists and vectors

In this task, you should complete the function `void task3()` in `task3.cpp`.

Consider a large object:

```c++
class LargeObject {
public:
    LargeObject();
    virtual ~LargeObject();
private:
    int m_data[500000];
};
LargeObject::LargeObject() {};
LargeObject::~LargeObject() {};
```

Measure times for putting such elements into a list and a vector!
The implementation for list have been provided. Try to complete the rest part for vector.

You may compile and run `task3.cpp` before you start. You may want to fix some bugs you made in task 2 first?

When you finish, type

    g++ task3.cpp Timer.cpp -o task3

in the console to compile, and get `task3` or `task3.exe`.

Then run (for Windows cmd)

    task3.exe

to see the result.

What can you observe? Can you draw a conclusion which one performs better?

---
## Task 4 - The mean and the median

The easiest way to measure the time usage of two methods is to compute the mean. But there might be extreme numbers in the time list!
How to obtain a measure of the time complexity regardless of occasional copying?
The answer is median.

Median can be computed by sorting and taking  the "center" value. Sort the lap­‐times, and print the mean and median times into the console.

Copy your code from task 3 and finish the computation of mean and median in `task4.cpp`.

When you finish, type

    g++ task4.cpp Timer.cpp -o task4

in the console to compile, and get `task4` or `task4.exe`.

Then run (for Windows cmd)

    task4.exe

to see the result.

What can you observe? Will you change your conclusion?

---
## Task 5 - Monte Carlo Agent With Timer!

Monte Carlo Agent has upgraded today! Last time it struggles to compute a fixed number of games to make a decision. Thanks god, you have just built a timer and it can compute in a fixed time period which makes things more efficient.

Now equip him with the timer!

For Monte Carlo Agent With Timer, you should
1. From current game state, take a valid action.
2. After taking the action, play the game randomly for a fixed time period. 
Here you should use [ `double MonteCarloAgentWithTimer::getTimeLimit()` ] to get 
the total time you have for simulation. This means that for each action, the posible
time to use is no longer than the total time divided by the number of actions.
3. Repeat step 1 for many times until you have tried all the possible actions.
4. Return the action that get the highest average score during your random play.

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp Timer.cpp -o main

in the console to compile, and get `main` or `main.exe`.

Then run (for Windows cmd)

    main.exe -A MonteCarloAgentWithTimer

to see the result. 

You should find that now it can always achieve 4096 in a rather short time. Think about why it will have a better performance!

For more fun, try the arguments:

    -A: choose an agent to play
        type: std::string
        valid range: { RandomAgent, MonteCarloAgent, MonteCarloAgentWithTimer }
        Default: MonteCarloAgentWithTimer
    -s: set the random seed
        type: int
        Default: time(NULL)
    -t: set the trial time (only work when MonteCarloAgent)
        type: int
        Default: 100
    -l: set the time limit in seconds (only work when MonteCarloAgentWithTimer)
        type: double
        Default: 0.01