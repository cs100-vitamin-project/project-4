#include <stdlib.h>
#include <iostream>
#include <chrono>

// Helper functions for task 1.
// Long operation to time
int dummyFunction(int n);

int fib(int n) {
    return dummyFunction(n);
}

int dummyFunction(int n) {
    if (n < 2) {
      return n;
    } else {
      return fib(n-1) + fib(n-2);
    }
}


void task1() {
    /* ****************** TASK 1  std::chrono ********************** *
     * Follow the instructions to test how much time it could take   *
     * by running dummyFunction(40);                                 *
     *                                                               *
     * Filling the places with ...                                   *
     * ************************************************************* */

    // Set the start time
    ... start_time = ...

    dummyFunction(40);

    // Set the end time
    ... end_time = ...

    // Set the duration
    ... time = end_time - start_time;

    // Output the time in microsecond
    std::cout << "fib(40) took " <<
    ... << " microsecond to run.\n";
}

int main(int argc, char *argv[])
{
    task1();

    return 0;
}