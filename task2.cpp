#include <stdlib.h>
#include <iostream>
#include "Timer.hpp"

// Helper functions for task 2.
// Long operation to time
int dummyFunction(int n);

int fib(int n) {
    return dummyFunction(n);
}

int dummyFunction(int n) {
    if (n < 2) {
      return n;
    } else {
      return fib(n-1) + fib(n-2);
    }
}

void task2() {
    /* *********** TASK 2  Implement a Timer class ***************** *
     * Create and fill in Timer.hpp and Timer.cpp.                   *
     * When you finish, compile and run task2.cpp and see how much   *
     * time dummyFunction(40) takes!                                 *
     * ************************************************************* */

    Timer timer(true);
    dummyFunction(40);
    timer.stop();

    std::cout << "fib(40) took " << timer.averageTime() << " seconds to run.\n";
}

int main(int argc, char *argv[])
{
    task2();

    return 0;
}