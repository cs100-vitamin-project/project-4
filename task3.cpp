#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include "Timer.hpp"


// Helper class for task 3.
class LargeObject {
public:
    LargeObject();
    virtual ~LargeObject();

private:
    int m_data[500000];
};

LargeObject::LargeObject() {};
LargeObject::~LargeObject() {};

void task3() {
    /* ************* TASK 3  Filling lists and vectors ************* *
     * Measure times for putting elements into a list and a vector!  *
     * The implementation for list have been provided. Try to        *
     * complete the rest part for vector!                            *
     *                                                               *
     * You may want to fix some bugs you made in task 2?             *
     * What can you observe? Which one performs better?              *
     * ************************************************************* */

    LargeObject obj;

    //create a list and a timer for the list
    std::list<LargeObject> myList;
    Timer listTimer;
    for( int i = 0; i < 500; i++ ) {
        listTimer.start();
        myList.push_back(obj);
        listTimer.stop();
    }

    //create a vector and a timer for the vector
    
    // YOUR CODE HERE

    //print all times for the list
    std::cout << "The times for the list are:\n";
    std::list<double>::iterator it1 = listTimer.begin();
    while( it1 != listTimer.end() ) {
        std::cout << *it1 << " "; it1++;
    }
    std::cout << "\n\n";

    //print all times for the vector
    
    // YOUR CODE HERE
    
}

int main(int argc, char *argv[])
{
    task3();

    return 0;
}