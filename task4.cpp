#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include "Timer.hpp"


// Helper class for task 4.
class LargeObject {
public:
    LargeObject();
    virtual ~LargeObject();

private:
    int m_data[500000];
};

LargeObject::LargeObject() {};
LargeObject::~LargeObject() {};

void task4() {
    /* ************* TASK 4  Filling lists and vectors ************* *
     * Find the mean and median of the data in Task 3!               *
     * Copy your code from task 3 and finish the computation of      *
     * mean and median.                                              *
     *                                                               *
     * What can you observe? Will you change your conclusion?        *
     * ************************************************************* */

    LargeObject obj;

    //create a list and a timer for the list
    std::list<LargeObject> myList;
    Timer listTimer;
    for( int i = 0; i < 500; i++ ) {
        listTimer.start();
        myList.push_back(obj);
        listTimer.stop();
    }

    //create a vector and a timer for the vector
    
    // YOUR CODE HERE

    listTimer.sort();
    vectorTimer.sort();

    //print all times for the list
    std::cout << "The times for the list are:\n";
    std::list<double>::iterator it1 = listTimer.begin();
    while( it1 != listTimer.end() ) {
        std::cout << *it1 << " "; it1++;
    }
    std::cout << "\n\n";

    //print all times for the vector
    
    // YOUR CODE HERE

    //print the means
    std::cout << "putting an element into a list takes in average " << ... << " seconds.\n";
    std::cout << "putting an element into a vector takes in average " << ... << " seconds.\n";

    //the median of the times is
    ...
    std::cout << "the median time for putting an element into a list is " << ... << " seconds.\n";
    ...
    std::cout << "the median time for putting an element into a vector is " << ... << " seconds.\n";
}

int main(int argc, char *argv[])
{
    task4();

    return 0;
}